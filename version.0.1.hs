import System.IO
import Player
--import Item
--import Data.Maybe
import Location

main = do
    startGame


data State = Normal { current :: Player, currLoc :: Location }
           | Invalid { prev :: Player}
           | Terminated

readCommand :: IO Char
readCommand = do
    hPutStr stderr "Enter [f]orward, [l]ook around, [i]tem drop, [q]uit, [t]ake:  "
    fmap head getLine

readInput :: IO State
readInput = do
    hPutStr stderr "Please enter your name:  "
    n <- fmap read getLine
   -- hPutStr $ "Welcome to Haskell Crashers "
    hPutStr stderr "Enter the Location number: (hint: it's 1) "
    l <- fmap read getLine
    return $ Normal (Player n l (1)) (Location n l (1))

displayDescription :: State -> IO ()
displayDescription Terminated = return ()
displayDescription (Normal r t) = putStrLn $ "\nThe player is " ++(show r)
displayDescription (Invalid r) = do
    putStrLn "\nError: Invalid operaation."
    putStrLn $ "\nThe operation is "++(show r)

updateGame :: State -> Char -> State
updateGame Terminated _ = Terminated
updateGame st op
    | op == 'l' = Normal (Player n l (1)) (Location n l (1))
    | op == 'f' = Normal (Player n l (1)) (Location n l (1))
    | op == 't' = Normal (Player n l (1)) (Location n l (1)) 
    | op == 'i' = Normal (Player n l (1)) (Location n l (1))
    | op == 'q' = Terminated
    | otherwise = Invalid play
    where play@(Player n l _) = case st of (Normal  n) -> n
                                           (Invalid n) -> n

theLoop :: State -> IO ()
theLoop Terminated = return ()
theLoop st = do
