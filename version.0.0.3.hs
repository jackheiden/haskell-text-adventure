import System.IO
import Player
import Location
import World
import Command

data State = Normal { current :: Command, getPlayer :: Player, getLocation :: Location }
	   | Invalid { previous :: Command }
	   | Terminated

type Item = String


takingItem :: Player -> Location -> (Player, Location)
takingItem p loc@(Location _ _ Nothing) = (p, loc)
takingItem p@(Player _ _ (Just _)) loc = (p, loc)
takingItem p loc = (p {inv = (locContents loc) }, loc {locContents = Nothing})


--need the starting location and direction


readGame = undefined

displayGame :: State -> IO ()
displayGame Terminated = return ()
displayGame (Normal c p l) = putStrLn $ "\nThe command is " ++ (show c)
displayGame (Invalid r) = do
	putStrLn "\nError: Invalid operation."
	putStrLn $ "\nThe command is "++(show r)

readCommand :: IO Char
readCommand = do
	hPutStr stderr "Enter [p]lay, [t]est, [q]uit: "
	fmap head getLine
	

updateGame :: State -> Char -> State
updateGame Terminated _ = Terminated
updateGame st op
    -- if operation is take
    -- then create a new game state that is like the old one except...
    --    it contains a player like the old player but holding a new item
    --    it cont a world like old one but with one location now missing an item
    | op == 't' = Normal (Command op)
    | op == 'q' = Terminated
    | otherwise = Invalid (Command op)
    --where play@(takingItem getPlayer getLocation) = case st of (Normal r) -> r		--i need to make
--                                        (Invalid r) -> r	--the function
--initialize all beginning values world,player, location
readInput :: IO Command
readInput = do
	hPutStr stderr "Please enter your name: "
	x <- fmap read getLine
	return $ Normal 
	where newPlayer = Player []

theLoop :: State -> IO ()
theLoop Terminated = return ()
theLoop st = do
	displayGame st
	c <- readCommand
	result <- return $ updateGame st c
	theLoop result

displayIntro :: IO()
displayIntro = putStrLn $ "\nHaskell Text Adventure v.0.0.3"
		       ++ "\n==============================\n"

displayOutro :: IO()
displayOutro = putStrLn $ "\n=============================="
		       ++ "\nCopyright (c) 2016 Jack Heiden\n"

main = do
	displayIntro
	initSt <- readInput
	theLoop initSt 
	displayOutro


