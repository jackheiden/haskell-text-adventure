module Player where
import Item
import Data.Maybe


--data Item = Item {currItem IO () :: MyMaybe Int}
data Item = Item {currItem :: String}

data Player = Player { playerName :: String,
                       currLoc :: Int, 
                       inventory :: Int }

instance Show Player where
    show (Player n l i) = (show n)++" is in "++(show l)
-- ++" and has "++(show i)




