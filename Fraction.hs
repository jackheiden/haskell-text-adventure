module Fraction where

data Fraction = Fraction { numer :: Int, denom :: Int }

instance Show Fraction where
    show (Fraction n d) = (show n)++"/"++(show d)

reciprocal :: Fraction -> Fraction
reciprocal (Fraction n d) = Fraction d n

simplify :: Fraction -> Fraction
simplify (Fraction n d) = Fraction (quot n c) (quot d c) where
    c = gcd n d
